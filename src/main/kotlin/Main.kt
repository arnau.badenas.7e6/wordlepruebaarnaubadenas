import java.io.File
import java.util.*
import kotlin.math.roundToInt
import kotlin.random.Random

/**
 * imprimirTaula -> retorna la variable de la taula en consola.
 * @param Array<Array<String>> taula feta d'una array bidimensional.
 * @author Arnau Badenas
 * @version 1.0
 */
fun imprimirTaula(taula: Array<Array<String>>){
    //Imprimeix taula
    for (row in taula) {
        for (col in row){
            print( "$col ")
        }
        println()
    }
    println("-----------------")
}

/**
 * escollirParaulaIComprovarRepetitsDiccionari -> Escollir la paraula dins del diccionari i comprovar si s'ha escollit abans.
 * Si és així, es tria un altre.
 * @param Array<String> Diccionari amb totes les paraules
 * @param MutableList<String> Llista amb totes les paraules ja utilitzades.
 * @return List<String> Paraula escollida separada en lletres.
 * @author Arnau Badenas
 * @version 1.0
 */
fun escollirParaulaIComprovarRepetitsDiccionari(diccionari:MutableList<String>,paraulesUtilitzades:MutableList<String>):List<String>{
    //Escollir paraula aleatòria de diccionari i afegir a paraulesUtilitzades
    //Genera un altre nombre aleatori mentre vegi que la paraula ja ha sigut resolta.
    var randNum:Int
    do {
        randNum = Random.nextInt(1, diccionari.size)
    }while (paraulesUtilitzades.contains(diccionari[randNum]))
    paraulesUtilitzades.add(diccionari[randNum])
    //Desglossar la paraula escollida i retornar-la.
    return diccionari[randNum].chunked(1)
}

/**
 * introduccioIComprovacioParaulaUsuari -> Demana la introducció i comprova la paraula introduida per l'usuari.
 * @param Scanner Escàner que llegeix les paraules introduides per l'usuari
 * @param Array<String> Diccionari amb totes les paraules disponibles
 * @return List<String> Paraula del usuari
 * @author Arnau Badenas
 * @version 1.0
 */
fun introduccioIComprovacioParaulaUsuari(reader:Scanner, diccionari:MutableList<String>):List<String>{
    var userInput:String
    do{
        var paraulaCorrecta = true
        println("Introduce una palabra")
        userInput = reader.next().uppercase(Locale.getDefault())
        //Partir paraula
        //Comprova llargada de la paraula
        val llargada = userInput.length
        if (llargada != 5){
            println("La palabra debe contener 5 carácteres! Prueba otra vez.")
            paraulaCorrecta = false
        }
        if (!diccionari.contains(userInput)){
            println("La palabra debe estar en el diccionario!")
            paraulaCorrecta = false
        }
    }while(!paraulaCorrecta)
    //Retorna la paraula introduïda per l'usuari
    return userInput.chunked(1)
}

/**
 * colorejatDeParaula -> Coloreja la paraula amb el color corresponent depenent si la lletra està al seu lloc o no està a la paraula destí.
 * @param Array<Array<String>> taula feta d'una array bidimensional.
 * @param List<String> paraula del usuari convertida en llista
 * @param List<String> paraula destí convertida en llista
 * @param Int intents restants de l'usuari
 * @author Arnau Badenas
 * @version 1.0
 */
fun colorejatDeParaula(taula:Array<Array<String>>, paraulaUser:List<String>, paraula:List<String>, intents:Int){
    //Colors
    val groc = "\u001b[33m" //33m
    val gris = "\u001b[37m"
    val verd = "\u001b[32m"
    val reset = "\u001b[0m"
    for(i in paraulaUser.indices){
        var lletraIgual = false
        //Comprovació de cada lletra de la paraula introduïda amb la paraula final (colorejar)
        //Lletra correcta
        if(paraulaUser[i] == paraula[i]){
            taula[intents-1][i] = verd + paraulaUser[i] + reset
        }
        else{
            for(j in paraula.indices){
                //Correcte pero a un lloc diferent (si la paraula del diccionari conté la lletra de l'input del usuari)
                if (paraula.contains(paraulaUser[j]) && !lletraIgual){
                    taula[intents-1][j] = groc + paraulaUser[j] + reset
                    lletraIgual = true
                }
                //Incorrecte
                else{
                    taula[intents-1][j] = gris + paraulaUser[j] + reset
                }
            }
        }
    }
}

/**
 * imprimirParaulaFinal -> Mostra la paraula final per consola
 * @param List<String> Paraula escollida a endevinar
 * @author Arnau Badenas
 * @version 1.0
 */
fun imprimirParaulaFinal(paraula:List<String>){
    //Imprimeix paraula final
    print("La palabra final era: ")
    for(i in paraula.indices){
        print(paraula[i])
    }
    println("\n-----------------------------------")
}
fun compararMillorRatxaIImprimirValor(ratxaDeParaules: Int) {
    //Ratxa de paraules encertades seguides actual
    println("Racha Actual: $ratxaDeParaules")

    //Millor ratxa
    if(ratxaDeParaules>millorRatxa){
        println("Mejor Racha: $ratxaDeParaules\n")
    }

    File("./src/estadistiques/estadistiques.txt").appendText("Ratxa de paraules: $ratxaDeParaules\n-*_*_*_*_*_*_*_*_*_*_*_*_*_*-\n")

}

/**
 * calcularIImprimirEstadistiques -> Calcula les estadistiques d'aquesta partida i les mostra
 * @param Int Número de partides
 * @param MutableList<String> Llista de paraules resoltes
 * @param Array<String> Diccionari amb totes les paraules disponibles
 * @param MutableList<Int> Matriu amb el número de vegades que s'ha encertat la paraula en cada intent
 * @param Int Ratxa de paraules seguides acertades
 * @author Arnau Badenas
 * @version 1.0
 */
fun calcularIImprimirEstadistiques(numPartides:Int, paraulesResoltes:MutableList<String>, diccionari: MutableList<String>, totalIntents:MutableList<Int>, ratxaDeParaules:Int){
    //Colors
    val blau = "\u001b[34m"
    val verdBackground = "\u001b[42m"
    val reset = "\u001b[0m"
    var estadistiquesAAfegir = ""

    //Mostrar estadístiques de jugador
    println(blau + "ESTADÍSTICAS DEL JUGADOR:" + reset)
    //Partides jugades
    println("Jugadas: $numPartides")
    estadistiquesAAfegir += "Número de partidas: $numPartides\n"

    //Quantitat de paraules resoltes
    val numParaulesResoltes = paraulesResoltes.size
    println("Has resuelto $numParaulesResoltes palabras!!")
    estadistiquesAAfegir += "Palabras resueltas: $numParaulesResoltes\n"

    //Quantitat de paraules NO resoltes
    val paraulesNoResoltes:Int = diccionari.size - paraulesResoltes.size
    println("Te ha faltado resolver $paraulesNoResoltes palabras.")
    estadistiquesAAfegir += "Palabras por resolver: $paraulesNoResoltes\n"

    //Percentatge de paraules que resolem
    val percentatgeResolt:Double = ((paraulesResoltes.size.toDouble()/numPartides.toDouble())*100.00)
    println("Victorias: ${percentatgeResolt.roundToInt()}%")
    estadistiquesAAfegir += "Victorias: ${percentatgeResolt.roundToInt()}%\n"

    File("./src/estadistiques/estadistiques.txt").appendText(estadistiquesAAfegir)
    compararMillorRatxaIImprimirValor(ratxaDeParaules)

    //Mitjana d'intents que necessitem
    println("Media de aciertos:")
    for (i in 0..5){
        val totalIntent = totalIntents[i]
        val mitjanaIntents = (totalIntent.toDouble()/paraulesResoltes.size.toDouble())*100.00
        //Si no s'encerta cap paraula paraulesResoltes seria 0, i si divideix per 0 donaria NaN, per tant:
        if (mitjanaIntents.isNaN()){
            println("0%")
            File("./src/estadistiques/estadistiques.txt").appendText("Media de aciertos: $0%\n")

        }else{
            print("Intento ${i+1}: ${mitjanaIntents.roundToInt()}% ")
            File("./src/estadistiques/estadistiques.txt").appendText("Intento ${i+1}: ${mitjanaIntents.roundToInt()}%\n")
            for (j in 1..totalIntent){
                print("$verdBackground  $reset")
            }
            println()
        }
    }
    File("./src/estadistiques/estadistiques.txt").appendText("\n")
}

/**
 * comprovarTornarAJugar -> Demana a l'usuari si vol tornar a jugar, i comprova el resultat.
 * @param Scanner Escàner que llegeix les paraules introduides per l'usuari.
 * @param Int Número de partides.
 * @param Array<String> Diccionari amb totes les paraules disponibles.
 * @return String Resultat de si l'usuari vol reintentar jugar o no.
 * @author Arnau Badenas
 * @version 1.0
 */
fun comprovarTornarAJugar(reader: Scanner,numPartides: Int,diccionari: MutableList<String>):String{
    //Comprovació de si ho vol tornar a intentar
    var reintentar:String
    do {
        println("Quieres volver a intentarlo? (Y/N)")
        reintentar = reader.next()
    }while (reintentar != "Y" && reintentar != "N")
    if (reintentar == "N" && numPartides>=diccionari.size){
        println("Has terminado todas las palabras!")
    }
    return reintentar
}
//------------------TASQUES A FER UF3-------------------
//DICCIONARI
// reemplaçar el array diccionari per un arxiu amb totes les paraules.
// crear nous diccionaris amb 2 idiomes diferents.
// refactor de totes les funcions per funcionar amb aquests nous diccionaris.

//HISTORIAL DE PARTIDES
// demanar al usuari el seu nom
/* fer un historial de partides juntada amb el nom del usuari,
*  juntar-ho a un fitxer (append)*/

//NOVES OPCIONS MENÚ INICIAL
/* Crear un menú inicial amb diferents opcions:
   -Veure llista historic de partides (es fa al final de cada partida)
   -Seleccionar idioma*/

//PER ACABAR
// Comentar amb KDOC totes les funcions
//fer tests unitaris de totes les funcions
/**
 * menuEscollirIdioma -> Demana a l'usuari quin idioma vol fer servir i retorna la opció.
 * @return int Opció registrada per input de l'usuari
 * @author Arnau Badenas
 * @version 1.0
 */
fun menuEscollirIdioma():Int{
    var opcio:String
    do {
        println("Escull un idioma!\n" +
                "Introdueix un número entre 1 i 3 per escollir...\n" +
                "1. Català (Per defecte)\n" +
                "2. Castellano\n" +
                "3. English\n")
        opcio = readln()
    }while (opcio.toIntOrNull() == null)
    return opcio.toInt()
}

/**
 * escollirDiccionari -> Segons la opció escollida al menú d'escollir idioma s'estableix un idioma.
 * @param Int Opció escollida per l'usuari
 * @return String Idioma en format text
 * @author Arnau Badenas
 * @version 1.0
 */
fun escollirDiccionari(opcio:Int):String{
    var idioma = ""
    when(opcio){
        1 -> idioma= "Catala"
        2 -> idioma= "Castellano"
        3 -> idioma= "English"
    }
    return idioma
}

/**
 * actualitzarDiccionari -> Segons l'idioma escollit, s'actualitza el diccionari amb les paraules de l'arxiu corresponent.
 * @return MutableList<String> Diccionari amb totes les paraules de l'idioma escollit
 * @author Arnau Badenas
 * @version 1.0
 */
fun actualitzarDiccionari():MutableList<String>{
    val idioma = escollirDiccionari(menuEscollirIdioma())
    val diccionari = mutableListOf<String>()
    var filePath = ""
    when(idioma){
        "Catala" -> {
            filePath = "./src/diccionaris/catala.txt"
        }
        "Castellano" -> {
            filePath = "./src/diccionaris/castellano.txt"
        }
        "English" -> {
            filePath = "./src/diccionaris/english.txt"
        }
        else -> println("ERROR")
    }
    //Introduir idioma a les estadistiques
    File("./src/estadistiques/estadistiques.txt").appendText("idioma de la partida: $idioma\n")
    val file = File(filePath)
    file.forEachLine {
        diccionari+=it
    }
    return diccionari
}

/**
 * visualitzarEstadistiques -> Mostra les estadistiques globals de tots els jugadors en pantalla.
 * @author Arnau Badenas
 * @version 1.0
 */
fun visualitzarEstadistiques(){
    val arxiuEstadistiques = File("./src/estadistiques/estadistiques.txt")
    println("-----------ESTADISTIQUES GLOBALS ------------------")
    arxiuEstadistiques.forEachLine {
        println(it)
    }
}

//Variables globals
var millorRatxa = 0 //He creat aquesta perquè no puc reassignar el valor dins d'una funció.
fun main() {
    //Variables d'utilitat
    val reader = Scanner(System.`in`)
    //Introduir nom
    println("Introdueix el teu nom:")
    val nom = readln()
    File("./src/estadistiques/estadistiques.txt").appendText("---JUGADOR $nom---\n")
    val diccionari = actualitzarDiccionari()
    //Variables estadístiques de final partida
    val paraulesResoltes:MutableList<String> = mutableListOf()
    val totalIntents:MutableList<Int> = mutableListOf(0,0,0,0,0,0)
    var ratxaDeParaules = 0
    val paraulesUtilitzades:MutableList<String> = mutableListOf()
    var numPartides = 0

    //Bucle de repetició del joc, es repeteix mentre l'usuari vulgui
    do {
        //Variables de la partida
        var intents = 1
        val taula = arrayOf(arrayOf("▢", "▢", "▢", "▢", "▢"), arrayOf("▢", "▢", "▢", "▢", "▢"), arrayOf("▢", "▢", "▢", "▢", "▢"), arrayOf("▢", "▢", "▢", "▢", "▢"), arrayOf("▢", "▢", "▢", "▢", "▢"), arrayOf("▢", "▢", "▢", "▢", "▢"))
        val paraula = escollirParaulaIComprovarRepetitsDiccionari(diccionari,paraulesUtilitzades)
        println(paraula) //DEBUG (ho deixo per si es vol fer proves, el diccionari és molt gran.)
        println("---- Wordle! ----")
        do {
            imprimirTaula(taula)
            val paraulaUser = introduccioIComprovacioParaulaUsuari(reader,diccionari)
            colorejatDeParaula(taula,paraulaUser,paraula,intents)
            intents += 1
            //Funció per comprovació de paraula introduïda per l'usuari (de moment no ho faig perquè he de passar i retornar massa valors (estadístiques) i no és pràctic.
            // Ho faria amb classes, pero diria que no està permès com abans utilitzar funcions)
            //Comprova si la paraula del diccionari és igual a la paraula introduïda per l'usuari.
            if (paraulaUser == paraula){
                println("Has ganado!!")
                totalIntents[intents-2]+=1
                paraulesResoltes.add(paraula.toString())
                ratxaDeParaules=+1
                intents = 7
            }
        } while (intents <= 6)
        numPartides +=1
        //Imprimeix taula final
        println("Aqui tienes la tabla final. Compartela con tus amigos!")
        imprimirTaula(taula)
        imprimirParaulaFinal(paraula)
        calcularIImprimirEstadistiques(numPartides,paraulesResoltes,diccionari,totalIntents,ratxaDeParaules)
        val reintentar = comprovarTornarAJugar(reader,numPartides,diccionari)
    }while (reintentar == "Y" && numPartides<=diccionari.size)
    println("Se ha acabado el juego! Espero que hayas disfrutado.")
    visualitzarEstadistiques()
}