import java.io.File
import java.text.Normalizer
import java.util.*

fun main(){
    transformarTextCatala()
}

/**
 * transformarTextAngles -> Fa totes les funcions necessàries per transformar el text del diccionari anglés al format
 * necessari per a poder ser treballat.
 * - Passar a majuscula.
 */
fun transformarTextAngles(){
    val filePath = "./src/diccionaris/english.txt"
    val file = File(filePath)
    var text = ""
    file.forEachLine {
        text+="${it.uppercase(Locale.getDefault())}\n"
    }
    file.writeText(text)
}
/**
 * transformarTextCastella -> Fa totes les funcions necessàries per transformar el text del diccionari anglés al format
 * necessari per a poder ser treballat.
 * - Passar a majuscula.
 */
fun transformarTextCastella(){
    val filePath = "./src/diccionaris/castellano.txt"
    val file = File(filePath)
    var text = ""
    file.forEachLine {
        text+="${it.uppercase(Locale.getDefault())}\n"
    }
    file.writeText(text)
}

/**
 * transformarTextCatala -> Fa totes les funcions necessàries per transformar el text del diccionari anglés al format
 * necessari per a poder ser treballat.
 * - Treure accents.
 */
private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()
fun transformarTextCatala(){

    fun CharSequence.unaccent(): String {
        val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
        return REGEX_UNACCENT.replace(temp, "")
    }
    val filePath = "./src/diccionaris/catala.txt"
    val file = File(filePath)
    var text = ""


    file.forEachLine {
        if (it.length == 5){
            var paraulaNova = it.unaccent()
            text+="$paraulaNova\n"
        }
    }
    file.writeText(text)
}