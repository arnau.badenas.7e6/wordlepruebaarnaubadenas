import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.io.File
import kotlin.random.Random

internal class MainKtTest {

    @Test
    fun checkEscollirParaulaDiccionari() {
        //Escollir paraula aleatòria de diccionari i afegir a paraulesUtilitzades
        //Genera un altre nombre aleatori mentre vegi que la paraula ja ha sigut resolta.
        val diccionari = arrayOf("Paraula1","Paraula2")
        val paraulesUtilitzades = mutableListOf<String>()
        var randNum:Int
        do {
            randNum = Random.nextInt(1, diccionari.size)
        }while (paraulesUtilitzades.contains(diccionari[randNum]))
        paraulesUtilitzades.add(diccionari[randNum])
        //Desglossar la paraula escollida i retornar-la.
        val expected = diccionari[randNum]

        assertTrue(expected in diccionari)
    }

    @Test
    fun checkIntroduccioIComprovacioParaulaUsuari() {
        var userInput:String
        val diccionari = arrayOf("Holaa","Adeeu")
        do{
            var paraulaCorrecta = true
            println("Introduce una palabra")
            userInput = "Holaa"
            //Partir paraula
            //Comprova llargada de la paraula
            val llargada = userInput.length
            if (llargada != 5){
                println("La palabra debe contener 5 carácteres! Prueba otra vez.")
                paraulaCorrecta = false
            }
            if (!diccionari.contains(userInput)){
                println("La palabra debe estar en el diccionario!")
                paraulaCorrecta = false
            }
        }while(!paraulaCorrecta)
        assertTrue(userInput in diccionari)
        assertTrue(userInput.length == 5)
    }

    @Test
    fun checkComprovarTornarAJugar() {
        val numPartides = 3
        val diccionari = arrayOf("Hola!","Adeu!")
        var reintentar:String
        do {
            println("Quieres volver a intentarlo? (Y/N)")
            reintentar = "Y"
        }while (reintentar != "Y" && reintentar != "N")
        if (reintentar == "N" && numPartides>=diccionari.size){
            println("Has terminado todas las palabras!")
        }
        assertTrue(numPartides>=diccionari.size)
    }

    @Test
    fun checkMenuEscollirIdioma() {
        var opcio:String
        val opcions = arrayOf(1,2,3)
        do {
            opcio = "1"
        }while (opcio.toIntOrNull() == null)
        val opcioRetornada = opcio.toInt()

        assertEquals(1,opcioRetornada)
        assertTrue(opcioRetornada in opcions)
        assertFalse(opcioRetornada == null)
    }

    @Test
    fun checkEscollirDiccionari() {
        var opcio = 1
        var idioma = ""
        when(opcio){
            1 -> idioma= "Catala"
            2 -> idioma= "Castellano"
            3 -> idioma= "English"
        }
        assertEquals(idioma,"Catala")
        assertNotEquals(idioma,"English")
    }

    @Test
    fun checkActualitzarDiccionari() {
        val idioma = "Catala"
        var diccionari = mutableListOf<String>()
        var filePath = ""
        when(idioma){
            "Catala" -> {
                filePath = "./src/diccionaris/catala.txt"
            }
            "Castellano" -> {
                filePath = "./src/diccionaris/castellano.txt"
            }
            "English" -> {
                filePath = "./src/diccionaris/english.txt"
            }
            else -> println("ERROR")
        }
        //Introduir idioma a les estadistiques
        File("./src/estadistiques/estadistiques.txt").appendText("idioma de la partida: $idioma\n")
        val file = File(filePath)
        file.forEachLine {
            diccionari+=it
        }

        assertTrue(diccionari.isNotEmpty())
    }
}